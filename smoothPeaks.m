function [ sData ] = smoothPeaks( data,sens,flag )
% This function provide a way to smooth some scattered experimental data.
% It uses the "Adiacent Averaging Method". 
%
% You can adjust the sensitiveness of the method setting the "sens" variable
% to a number from 1 to 3. More the sensitiveness more the smoothing, but
% if it is too much the data can differ too much from the experimental one
%
%
%
% [ INPUTS ]
% - data: the array of the experimental data
% - (opt) sens: sensitiveness value (default = 1)
% - (opt) flag: set to one for plots (default = 0)
%
% [ OUTPUTS ]
% - sData: smoothed data array
%
%
%
% Author: Ruben Di Battista - IPTL Mission Analysis 
% Skyward Experimental Rocketry - 2013

if nargin <3
    flag = 0;
elseif nargin <2
    sens = 1;
end

% nS2 points around the point to smooth
n=numel(data);

switch sens
    case 2
        nS2 = 2*ceil(n/500);
    case 3
        nS2 = 2*ceil(n/100);
    otherwise
        nS2 = 2*ceil(n/1000);
end


if flag
    figure;
    plot(data);
end

% Smoothing routine
% Borders
for i=1:nS2/2
    data(i) = mean(data([1:i-1 i+1:nS2/2]));
    data(n-i) = mean(data([n-nS2/2:n-i-1 n-1+1:n]));
end

%Center of the array
for i=nS2/2+1:n-nS2/2-1
    data(i) = mean(data([i-nS2/2:i-1 i+1:i+nS2/2]));
end

if flag
    hold on
    plot(data,'r--','LineWidth',1.2);
    title('Experimental vS Smoothed');
    hold off
end

sData = data;

end

