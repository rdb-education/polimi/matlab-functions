function [ l ] = powerEigs(A,tol,flag,mu )
% Function for calculation of min, max or shifted eigen value.
% l= powerEigs(A,tol,flag,mu )
% %%%% INPUT
% - A: matrix which eigen value has to be calculated of
% - tol: Tolerance.
% - mu: this is the nearest value where to search for the eigen value. If
% it is set to 0 -> minimun eigen value searched. 
% - flag: If it is set to 1, sparse A matrix has to be given and sparse
% matrices operations are used (default = 0);
% %%%% OUTPUTS
% - l: the eigen value searched.



[n,m] = size(A);

% Error handling

narginchk(2,4);

if not( n == m) 
    error('Just for square matrices!');
end

if nargin == 2
    flag = 0;
end

if nargin > 3
    shift = 1;
else
    shift = 0;
end

if shift
    e = ones(n,1);
    %Sparse matrices management
    if flag
        identity = spdiags(e,0,n,n);
        A = A - mu*identity;
        [L,U] = ilu(A);
    else
        identity = diag(e);
        A = A - mu*identity;
        [L,U] = lu(A);
    end
else
    if flag
        [L,U] = ilu(A);
    else
        [L,U] = lu(A);
    end
end

% Loop
err = tol + eps();
it = 0;
nmax = 1e4;
X0 = rand(n,1);
Y0 = X0/norm(X0);
l0 = 0;

while not( (err<tol || it > nmax) )
    
    if shift
        
        y = L\Y0;
        X = U\y;
    else
        X = A*Y0;
    end
    
    Y = X/norm(X);
    l = Y'*X;
    
    % Update
    err = abs(l-l0);
    X0 = X;
    Y0 = Y;
    l0 = l;
    it = it+1;
end

if shift
    l = 1/l + mu;
end
   
s=sprintf('Converged in %d iterations \n',it);
disp(s);

end


