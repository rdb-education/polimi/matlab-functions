function [ B ] = SwitchColumn( A,numcol1,numcol2 )
%This switch a column with another column
[row,col] = size(A);
%Clone the matrix
B = A;
 for i=1:row
     %Switch Column
     B(i,numcol1) = A(i,numcol2);
     B(i,numcol2) = A(i,numcol1);
 end

end

