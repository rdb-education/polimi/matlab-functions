function [ zero,it, xvect ] = newton_p(x0,toll,tollder,fun,mol)
%Applica il metodo di Newton per la ricerca degli  zeri di una funzione%
%__________________Variabili in ingresso____________________%
% ------ x0 : Guess Iniziale
% ----- toll: Tolleranza Accettata (consigliato 10^-6)
% ----- tollder: Tolleranza Accettata per derive.m
% ----- fun : la funzione in ingresso definita inline o @
% ----- mol : molteplicit� supposto dello zero
%
if (nargin < 4 )
    mol = 1
end
    it = 1; %Inizio a contare le iterate
    scost = abs(x0); %Lo scostamento iniziale � pari al guess
    while ( not(scost < toll))

        xk = x0 - mol * (fun(x0))/(derive(fun,x0,tollder)); %Algoritmo Newton
        xvect(it) = xk; %Assegno al vettore l'iterata i-esima
        scost = abs(xk - x0); %Calcolo lo scostamento
        x0 = xk; %Aggiorno l'iterata
        it = it+1;
    end
    
    it = it -1; %Sono partito da 1
    xvect = xvect';%Display in colonna!
    
    %Calcolo l'ultimo elemento delle iterate che � lo zero
    [row,col] = size(xvect);
    zero = xvect(col);
        



end

