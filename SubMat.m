function [ B ] = SubMat( A )
[row,col] = size(A);

if (row == col)
    if(not(row ==2))
        %Genero Sotto-Matrice Laplace
        for i=1:row-1
        
            for j=1:col-1
             B(i,j) = A(i+1,j+1);
            end
        end
    end
end

